/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_can.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_spi.h"
#include "stm32f0xx_dma.h"

#include "uniqueid.h"
			
#include <stdlib.h>
#include <string.h>

#include "iodefs.h"

#define TRUE 1
#define FALSE 0

volatile uint16_t heartbeatCounter = 0;

volatile uint8_t ledMatrixCol = 0;
volatile uint8_t ledMatrixFlag = 0;

volatile uint8_t keyMatrixCol = 0;
volatile uint8_t keyMatrixFlag = 0;

volatile uint8_t solenoidFlag = 0;

volatile uint8_t switchPollingFlag = 0;

volatile uint8_t ledBlinkFlag = 0;

volatile uint8_t dmaComplete = 1;

uint8_t powerStatusPollingInterval = 0;
uint8_t powerStatusPollingCount = 0;
volatile uint8_t powerStatusPollingFlag = 0;

static void small_delay(int delay);
void setup_io(void);
void setup_timer(void);
uint32_t read_dip_switch(void);

#define RECEIVE_BUFFER_LENGTH 16
CanRxMsg receiveQueue[RECEIVE_BUFFER_LENGTH];
volatile uint8_t receiveHead = 0;
volatile uint8_t receiveTail = 0;
uint8_t enqueueReceivedFrame(CanRxMsg *msgIn);
uint8_t dequeueReceivedFrame(CanRxMsg *msgOut);
uint8_t receiveQueueFull();
uint8_t receiveQueueEmpty();

#define NUM_PIXELS 16ul

#define NUM_PIXEL_SPI_BYTES ((NUM_PIXELS+2ul) * 24ul) / 2ul
static uint8_t pixelSPIArray[NUM_PIXEL_SPI_BYTES];

// random bytes generated from https://www.random.org/bytes/
const uint8_t hardwareSignature[] = {0xa3, 0x67, 0xa6, 0x67, 0xb9, 0x9c, 0x87, 0x87};

#define ENCODED_ONE 0b0011
#define ENCODED_ZERO 0b0111

uint8_t enqueueReceivedFrame(CanRxMsg *msgIn) {
	uint8_t i = (receiveHead + 1) % RECEIVE_BUFFER_LENGTH;
	if(i != receiveTail) {
		memcpy(&(receiveQueue[receiveHead]), msgIn, sizeof(CanRxMsg));
		receiveHead = i;
		return TRUE;
	}
	return FALSE;
}

uint8_t dequeueReceivedFrame(CanRxMsg *msgOut) {
	if(receiveQueueEmpty()) return FALSE;

	memcpy(msgOut, &(receiveQueue[receiveTail]), sizeof(CanRxMsg));
	receiveTail = (receiveTail + 1) % RECEIVE_BUFFER_LENGTH;
	return TRUE;
}

uint8_t receiveQueueFull() {
	if(((receiveHead + 1) % RECEIVE_BUFFER_LENGTH) == receiveTail) {
		return TRUE;
	}
	return FALSE;
}

uint8_t receiveQueueEmpty() {
	if(receiveHead == receiveTail) {
		return TRUE;
	}
	return FALSE;
}

#define TRANSMIT_BUFFER_LENGTH 16
CanTxMsg transmitQueue[TRANSMIT_BUFFER_LENGTH];
volatile uint8_t transmitHead = 0;
volatile uint8_t transmitTail = 0;
uint8_t enqueueTransmitFrame(CanTxMsg *msgIn);
uint8_t dequeueTransmitFrame(CanTxMsg *msgOut);
uint8_t transmitQueueFull();
uint8_t transmitQueueEmpty();

uint8_t enqueueTransmitFrame(CanTxMsg *msgIn) {
	uint8_t i = (transmitHead + 1) % TRANSMIT_BUFFER_LENGTH;
	if(i != transmitTail) {
		memcpy(&(transmitQueue[transmitHead]), msgIn, sizeof(CanTxMsg));
		transmitHead = i;
		return TRUE;
	}
	return FALSE;
}

uint8_t dequeueTransmitFrame(CanTxMsg *msgOut) {
	if(transmitQueueEmpty()) return FALSE;

	memcpy(msgOut, &(transmitQueue[transmitTail]), sizeof(CanTxMsg));
	transmitTail = (transmitTail + 1) % TRANSMIT_BUFFER_LENGTH;
	return TRUE;
}

uint8_t transmitQueueFull() {
	if(((transmitHead + 1) % TRANSMIT_BUFFER_LENGTH) == transmitTail) {
		return TRUE;
	}
	return FALSE;
}

uint8_t transmitQueueEmpty() {
	if(transmitHead == transmitTail) {
		return TRUE;
	}
	return FALSE;
}

struct lampSettings {
	uint8_t currentState;
	uint8_t onState;
	uint8_t onTime;
	uint8_t offState;
	uint8_t offTime;
	uint8_t defaultOnState;
	uint8_t defaultOnTime;
	uint8_t defaultOffState;
	uint8_t defaultOffTime;
	uint8_t messageTriggerMask;
	uint8_t timeCounter;
};

struct lampSettings lamps[16];

void initializeLamps() {
	uint8_t i;

	for(i=0; i<16; i++) {
		lamps[i].currentState = 0;
		lamps[i].defaultOnState = 255;
		lamps[i].defaultOnTime = 0;
		lamps[i].defaultOffState = 0;
		lamps[i].defaultOffTime = 0;
		lamps[i].messageTriggerMask = 0;
		lamps[i].timeCounter = 0;
	}

	return;
}

struct switchRule {
	uint8_t priority;
	uint8_t address;
	uint8_t featureType;
	uint8_t featureNumber;
	uint8_t messageType;
	uint8_t dlc;
	uint8_t data[8];
};

struct switchSettings {
	uint8_t currentState;
	uint8_t lastState;
	uint8_t messageTriggerMask;
	uint8_t pollingInterval;
	uint8_t pollingCounter;
	uint8_t debounceLimit;
	uint8_t debounceCounter;
	uint8_t closeRuleEnabled;
	struct switchRule closeRule;
	uint8_t openRuleEnabled;
	struct switchRule openRule;
};

struct switchSettings switches[16];

void sendSwitchStatus(uint8_t priority, uint8_t switchNum);
void initializeSwitches() {
	uint8_t i;

	for(i=0; i<16; i++) {
		switches[i].currentState = 0;
		switches[i].lastState = 0;
		switches[i].messageTriggerMask = 0x03;
		switches[i].pollingInterval = 0;
		switches[i].pollingCounter= 0;
		switches[i].debounceLimit = 10;
		switches[i].debounceCounter = 0;
		switches[i].closeRuleEnabled = 0;
		switches[i].openRuleEnabled = 0;
	}

	return;
}

struct solenoidSettings {
	uint8_t currentState;
	uint8_t lastState;
	uint8_t messageTriggerMask;
	uint8_t onDuration;
	uint8_t defaultOnDuration;
	uint8_t timeCounter;
};

struct solenoidSettings solenoids[8];

void initializeSolenoids() {
	uint8_t i;

	for(i=0; i<8; i++) {
		solenoids[i].currentState = 0;
		solenoids[i].lastState = 0;
		solenoids[i].messageTriggerMask = 0;
		solenoids[i].onDuration = 0;
		solenoids[i].defaultOnDuration = 0;
		solenoids[i].timeCounter = 0;
	}
}

struct rgbSettings {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

struct rgbSettings rgbLEDs[16];


void initializeRGB() {
	uint8_t i;

	for(i=0; i<16; i++) {
		rgbLEDs[i].red = 0;
		rgbLEDs[i].green = 0;
		rgbLEDs[i].blue = 0;
	}
}


void pixels_buildSPIArray() {
	uint16_t i, j, k;
	uint8_t tempR, tempG, tempB;
	uint32_t encodedR = 0xFFFFFFFFUL, encodedG = 0xFFFFFFFFUL, encodedB = 0xFFFFFFFFUL;

	k = 0;

	pixelSPIArray[k] = encodedG >> 24 & 0xFF;
	pixelSPIArray[k+1] = encodedG >> 16 & 0xFF;
	pixelSPIArray[k+2] = encodedG >> 8 & 0xFF;
	pixelSPIArray[k+3] = encodedG & 0xFF;

	pixelSPIArray[k+4] = encodedR >> 24 & 0xFF;
	pixelSPIArray[k+5] = encodedR >> 16 & 0xFF;
	pixelSPIArray[k+6] = encodedR >> 8 & 0xFF;
	pixelSPIArray[k+7] = encodedR & 0xFF;

	pixelSPIArray[k+8] = encodedB >> 24 & 0xFF;
	pixelSPIArray[k+9] = encodedB >> 16 & 0xFF;
	pixelSPIArray[k+10] = encodedB >> 8 & 0xFF;
	pixelSPIArray[k+11] = encodedB & 0xFF;

	for(i = 0; i < NUM_PIXELS; i++) {
		// for each pixel
		tempR = rgbLEDs[i].red;
		tempG = rgbLEDs[i].green;
		tempB = rgbLEDs[i].blue;
		encodedR = 0;
		encodedG = 0;
		encodedB = 0;
		k = (i * 12) + 12;

		for(j=0; j < 8; j++) {
			// for each bit in R, G, B
			encodedR <<= 4;
			encodedG <<= 4;
			encodedB <<= 4;

			encodedR |= (tempR & 0x80) ? ENCODED_ONE : ENCODED_ZERO;
			encodedG |= (tempG & 0x80) ? ENCODED_ONE : ENCODED_ZERO;
			encodedB |= (tempB & 0x80) ? ENCODED_ONE : ENCODED_ZERO;

			tempR <<= 1;
			tempG <<= 1;
			tempB <<= 1;
		}

		pixelSPIArray[k] = encodedG >> 24 & 0xFF;
		pixelSPIArray[k+1] = encodedG >> 16 & 0xFF;
		pixelSPIArray[k+2] = encodedG >> 8 & 0xFF;
		pixelSPIArray[k+3] = encodedG & 0xFF;

		pixelSPIArray[k+4] = encodedR >> 24 & 0xFF;
		pixelSPIArray[k+5] = encodedR >> 16 & 0xFF;
		pixelSPIArray[k+6] = encodedR >> 8 & 0xFF;
		pixelSPIArray[k+7] = encodedR & 0xFF;

		pixelSPIArray[k+8] = encodedB >> 24 & 0xFF;
		pixelSPIArray[k+9] = encodedB >> 16 & 0xFF;
		pixelSPIArray[k+10] = encodedB >> 8 & 0xFF;
		pixelSPIArray[k+11] = encodedB & 0xFF;
	}

	k = (17 * 12);

	encodedG = 0xFFFFFFFFUL;
	encodedR = 0xFFFFFFFFUL;
	encodedB = 0xFFFFFFFFUL;

	pixelSPIArray[k] = encodedG >> 24 & 0xFF;
	pixelSPIArray[k+1] = encodedG >> 16 & 0xFF;
	pixelSPIArray[k+2] = encodedG >> 8 & 0xFF;
	pixelSPIArray[k+3] = encodedG & 0xFF;

	pixelSPIArray[k+4] = encodedR >> 24 & 0xFF;
	pixelSPIArray[k+5] = encodedR >> 16 & 0xFF;
	pixelSPIArray[k+6] = encodedR >> 8 & 0xFF;
	pixelSPIArray[k+7] = encodedR & 0xFF;

	pixelSPIArray[k+8] = encodedB >> 24 & 0xFF;
	pixelSPIArray[k+9] = encodedB >> 16 & 0xFF;
	pixelSPIArray[k+10] = encodedB >> 8 & 0xFF;
	pixelSPIArray[k+11] = encodedB & 0xFF;
}

void pixels_sendUpdate() {
	if(dmaComplete == 1) {
		DMA_ClearFlag(DMA1_FLAG_GL3 | DMA1_FLAG_HT3 | DMA1_FLAG_TC3 | DMA1_FLAG_TE3);
		dmaComplete = 0;
		SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Tx, ENABLE);
		SPI_Cmd(SPI1, ENABLE);
		DMA_Cmd(DMA1_Channel3, ENABLE);
	}
}

uint32_t dipSwitches = 0;
uint8_t boardAddress = 0;

uint8_t lampBuffer[4][4];
uint8_t lampFrameBuffer[4];

uint8_t buttonStatus[16];
uint8_t newButtonStatus[16];

CanRxMsg msg_obj;
CanRxMsg allcall_obj;
CanTxMsg tx_msg_obj;

#define BUILD_ARB_FIELD(priority, bs, address, feature_type, feature_number, message_type) ((priority & 0x0f) << 25) | ((bs & 0x01) << 24) | (address << 16) | ((feature_type & 0x0f) << 12) | ((feature_number & 0x0f) << 8) | ((message_type & 0x0f) << 4)

volatile uint8_t waitForTXFinished = 0;
#define TX_MSGOBJ_NUM 5

void primeTransmitBuffer() {
	if(waitForTXFinished == 0) {
		if(!transmitQueueEmpty()) {
			dequeueTransmitFrame(&tx_msg_obj);
			waitForTXFinished = 5;
			CAN_Transmit(CAN, &tx_msg_obj);;
		}
	}
}

void clearLampBuffer() {
	uint8_t i, j;

	for(i=0; i<4; i++) {
		for(j=0; j<4; j++) {
			lampBuffer[i][j] = 0;
		}
	}
}

uint8_t checkLampBright(uint8_t lampNum) {
	if(lamps[lampNum].currentState) {
		return lamps[lampNum].onState;
	} else {
		return lamps[lampNum].offState;
	}
}

void buildLampFrameBuffer() {
	uint8_t newLampFrameBuffer[4];

	newLampFrameBuffer[0] = 0b11100000;
	newLampFrameBuffer[1] = 0b11010000;
	newLampFrameBuffer[2] = 0b10110000;
	newLampFrameBuffer[3] = 0b01110000;

	if(checkLampBright(0)) newLampFrameBuffer[0] |= 0b00000001;
	if(checkLampBright(4)) newLampFrameBuffer[1] |= 0b00000001;
	if(checkLampBright(8)) newLampFrameBuffer[2] |= 0b00000001;
	if(checkLampBright(12)) newLampFrameBuffer[3] |= 0b00000001;

	if(checkLampBright(1)) newLampFrameBuffer[0] |= 0b00000010;
	if(checkLampBright(5)) newLampFrameBuffer[1] |= 0b00000010;
	if(checkLampBright(9)) newLampFrameBuffer[2] |= 0b00000010;
	if(checkLampBright(13)) newLampFrameBuffer[3] |= 0b00000010;

	if(checkLampBright(2)) newLampFrameBuffer[0] |= 0b00000100;
	if(checkLampBright(6)) newLampFrameBuffer[1] |= 0b00000100;
	if(checkLampBright(10)) newLampFrameBuffer[2] |= 0b00000100;
	if(checkLampBright(14)) newLampFrameBuffer[3] |= 0b00000100;

	if(checkLampBright(3)) newLampFrameBuffer[0] |= 0b00001000;
	if(checkLampBright(7)) newLampFrameBuffer[1] |= 0b00001000;
	if(checkLampBright(11)) newLampFrameBuffer[2] |= 0b00001000;
	if(checkLampBright(15)) newLampFrameBuffer[3] |= 0b00001000;

	memcpy(lampFrameBuffer, newLampFrameBuffer, 4);
	return;
}

void writeLEDMatrix(uint8_t matrixData) {
	// Dead time insertion. This is not my best work.
	GPIO_ResetBits(LAMP_ROW0_PORT, LAMP_ROW0_PIN | LAMP_ROW1_PIN | LAMP_ROW2_PIN | LAMP_ROW3_PIN);
	GPIO_SetBits(LAMP_COL0_PORT, LAMP_COL0_PIN | LAMP_COL1_PIN | LAMP_COL2_PIN | LAMP_COL3_PIN);
	small_delay(30);
	GPIO_WriteBit(LAMP_COL3_PORT, LAMP_COL3_PIN, (matrixData & (1 << 7)));
	GPIO_WriteBit(LAMP_COL2_PORT, LAMP_COL2_PIN, (matrixData & (1 << 6)));
	GPIO_WriteBit(LAMP_COL1_PORT, LAMP_COL1_PIN, (matrixData & (1 << 5)));
	GPIO_WriteBit(LAMP_COL0_PORT, LAMP_COL0_PIN, (matrixData & (1 << 4)));
	GPIO_WriteBit(LAMP_ROW3_PORT, LAMP_ROW3_PIN, (matrixData & (1 << 3)));
	GPIO_WriteBit(LAMP_ROW2_PORT, LAMP_ROW2_PIN, (matrixData & (1 << 2)));
	GPIO_WriteBit(LAMP_ROW1_PORT, LAMP_ROW1_PIN, (matrixData & (1 << 1)));
	GPIO_WriteBit(LAMP_ROW0_PORT, LAMP_ROW0_PIN, (matrixData & (1 << 0)));
	return;
}

void processSwitchUpdate(uint8_t switchNum, uint8_t newState) {
	if(switches[switchNum].debounceCounter == 0) {
		switches[switchNum].lastState = switches[switchNum].currentState;
		switches[switchNum].currentState = newState;
		if(switches[switchNum].lastState != newState) {
			switches[switchNum].debounceCounter = switches[switchNum].debounceLimit;
		}
		if(switches[switchNum].messageTriggerMask & 0x01) {
			if(!(switches[switchNum].lastState) && (switches[switchNum].currentState)) {
				sendSwitchStatus(0x01, switchNum);
			}
		}
		if(switches[switchNum].messageTriggerMask & 0x02) {
			if((switches[switchNum].lastState) && !(switches[switchNum].currentState)) {
				sendSwitchStatus(0x01, switchNum);
			}
		}
	} else {
		switches[switchNum].debounceCounter--;
	}
}

const uint8_t switchRemap[] = {0x0F, 0x0B, 0x07, 0x03, 0x0E, 0x0A, 0x06, 0x02, 0x0D, 0x09, 0x05, 0x01, 0x0C, 0x08, 0x04, 0x00};

void readSwitchCol(uint8_t colNum) {

	switch(colNum) {
	case 0:
		GPIO_WriteBit(SWITCH_COL0_PORT, SWITCH_COL0_PIN, 0);
		GPIO_WriteBit(SWITCH_COL1_PORT, SWITCH_COL1_PIN, 1);
		GPIO_WriteBit(SWITCH_COL2_PORT, SWITCH_COL2_PIN, 1);
		GPIO_WriteBit(SWITCH_COL3_PORT, SWITCH_COL3_PIN, 1);
		break;
	case 1:
		GPIO_WriteBit(SWITCH_COL0_PORT, SWITCH_COL0_PIN, 1);
		GPIO_WriteBit(SWITCH_COL1_PORT, SWITCH_COL1_PIN, 0);
		GPIO_WriteBit(SWITCH_COL2_PORT, SWITCH_COL2_PIN, 1);
		GPIO_WriteBit(SWITCH_COL3_PORT, SWITCH_COL3_PIN, 1);
		break;
	case 2:
		GPIO_WriteBit(SWITCH_COL0_PORT, SWITCH_COL0_PIN, 1);
		GPIO_WriteBit(SWITCH_COL1_PORT, SWITCH_COL1_PIN, 1);
		GPIO_WriteBit(SWITCH_COL2_PORT, SWITCH_COL2_PIN, 0);
		GPIO_WriteBit(SWITCH_COL3_PORT, SWITCH_COL3_PIN, 1);
		break;
	case 3:
		GPIO_WriteBit(SWITCH_COL0_PORT, SWITCH_COL0_PIN, 1);
		GPIO_WriteBit(SWITCH_COL1_PORT, SWITCH_COL1_PIN, 1);
		GPIO_WriteBit(SWITCH_COL2_PORT, SWITCH_COL2_PIN, 1);
		GPIO_WriteBit(SWITCH_COL3_PORT, SWITCH_COL3_PIN, 0);
		break;
	default:
		return;
		break;
	}

	small_delay(1);

	processSwitchUpdate(switchRemap[0 + (colNum * 4)], !GPIO_ReadInputDataBit(SWITCH_ROW0_PORT, SWITCH_ROW0_PIN));
	processSwitchUpdate(switchRemap[1 + (colNum * 4)], !GPIO_ReadInputDataBit(SWITCH_ROW1_PORT, SWITCH_ROW1_PIN));
	processSwitchUpdate(switchRemap[2 + (colNum * 4)], !GPIO_ReadInputDataBit(SWITCH_ROW2_PORT, SWITCH_ROW2_PIN));
	processSwitchUpdate(switchRemap[3 + (colNum * 4)], !GPIO_ReadInputDataBit(SWITCH_ROW3_PORT, SWITCH_ROW3_PIN));

	return;
}

void decodeArbField(uint32_t arb_field, uint8_t *priority, uint8_t *bs, uint8_t *address, uint8_t *feature_type, uint8_t *feature_num, uint8_t *message_type, uint8_t *isRequest) {
	*priority = (arb_field & 0x1E000000) >> 25;
	*bs = (arb_field & 0x01000000) >> 24;
	*address = (arb_field & 0x00FF0000) >> 16;
	*feature_type = (arb_field & 0x0000F000) >> 12;
	*feature_num = (arb_field & 0x00000F00) >> 8;
	*message_type = (arb_field & 0x000000F0) >> 4;
	*isRequest = 0;

	return;
}

uint32_t ADCRead( uint8_t channelNum )
{
	return ( 0 );	/* return A/D conversion value */
}

void sendBoardID1(uint8_t priority) {
	CanTxMsg transmitObj;
	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x00);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 4;
	transmitObj.Data[0] = 0x00;
	transmitObj.Data[1] = 0x01;
	transmitObj.Data[2] = 0x02;
	transmitObj.Data[3] = 0x03;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void sendBoardID2(uint8_t priority) {
	CanTxMsg transmitObj;
	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x01);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 2;
	transmitObj.Data[0] = 0x01;
	transmitObj.Data[1] = 0x00;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void sendPowerStatus(uint8_t priority) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x02);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 8;

	transmitObj.Data[0] = (ADCRead(7) >> 2) & 0xFF;
	transmitObj.Data[1] = (ADCRead(5) >> 2) & 0xFF;
	transmitObj.Data[2] = 0x00;
	transmitObj.Data[3] = 0x00;
	transmitObj.Data[4] = (ADCRead(1) >> 2) & 0xFF;
	transmitObj.Data[5] = (ADCRead(2) >> 2) & 0xFF;
	transmitObj.Data[6] = (ADCRead(3) >> 2) & 0xFF;
	transmitObj.Data[7] = 0x00;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void sendPowerStatusPolling(uint8_t priority) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x03);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = powerStatusPollingInterval;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setPowerStatusPolling(uint8_t pollingValue) {
	powerStatusPollingInterval = pollingValue;
}

void sendSerialNumberA(uint8_t priority) {
	CanTxMsg transmitObj;

	uint32_t uid[2];

	uid[0] = UUID0;
	uid[1] = UUID1;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x04);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 8;
	memcpy((transmitObj.Data), uid, 8);

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void sendSerialNumberB(uint8_t priority) {
	CanTxMsg transmitObj;

	uint32_t uid[2];

	uid[0] = UUID2;
	uid[1] = 0;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x04);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 8;
	memcpy((transmitObj.Data), uid, 8);

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void sendBoardSignature(uint8_t priority) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x00, 0x00, 0x04);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 8;
	memcpy((transmitObj.Data), hardwareSignature, 8);

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void processIncomingSystemManagementFrame(CanRxMsg *incomingMsg) {
	uint8_t priority, bs, address, featureType, featureNum, messageType, isRequest;
	decodeArbField(incomingMsg->ExtId, &priority, &bs, &address, &featureType, &featureNum, &messageType, &isRequest);
	isRequest = (incomingMsg->RTR) & CAN_RTR_Remote ? 1 : 0;

	switch(messageType) {
	case 0x00: // Board ID 1 - requestable only
		if(isRequest) sendBoardID1(priority);
		break;
	case 0x01: // Board ID 2 - requestable only
		if(isRequest) sendBoardID2(priority);
		break;
	case 0x02: // Power status - requestable only
		if(isRequest) sendPowerStatus(priority);
		break;
	case 0x03: // Power Status Polling - set or request
		if(isRequest) {
			sendPowerStatusPolling(priority);
		} else {
			if(incomingMsg->DLC >= 1) setPowerStatusPolling(incomingMsg->Data[0]);
		}
		break;
	case 0x04: // Device serial number A
		if(isRequest) sendSerialNumberA(priority);
		break;
	case 0x05: // Device serial number B
		if(isRequest) sendSerialNumberB(priority);
		break;
	case 0x06: // Board signature
		if(isRequest) sendBoardSignature(priority);
		break;
	default:
		break;
	}

	return;
}

void sendSwitchStatus(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj, ruleTransmitObj;
	uint8_t ruleDidSend = 0;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x00);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 2;
	transmitObj.Data[0] = switches[switchNum].currentState;
	if(switches[switchNum].currentState) {
		if(switches[switchNum].lastState) {
			transmitObj.Data[1] = 0x00;
		} else {
			transmitObj.Data[1] = 0x01;
		}
		// send rule if present
		if(switches[switchNum].closeRuleEnabled) {
			ruleDidSend = 1;
			ruleTransmitObj.ExtId = BUILD_ARB_FIELD(switches[switchNum].closeRule.priority, 1, switches[switchNum].closeRule.address, switches[switchNum].closeRule.featureType, switches[switchNum].closeRule.featureNumber, switches[switchNum].closeRule.messageType);
			ruleTransmitObj.IDE = CAN_Id_Extended;
			ruleTransmitObj.RTR = CAN_RTR_Data;
			ruleTransmitObj.DLC = switches[switchNum].closeRule.dlc;
			ruleTransmitObj.Data[0] = switches[switchNum].closeRule.data[0];
			ruleTransmitObj.Data[1] = switches[switchNum].closeRule.data[1];
			ruleTransmitObj.Data[2] = switches[switchNum].closeRule.data[2];
			ruleTransmitObj.Data[3] = switches[switchNum].closeRule.data[3];
			ruleTransmitObj.Data[4] = switches[switchNum].closeRule.data[4];
			ruleTransmitObj.Data[5] = switches[switchNum].closeRule.data[5];
			ruleTransmitObj.Data[6] = switches[switchNum].closeRule.data[6];
			ruleTransmitObj.Data[7] = switches[switchNum].closeRule.data[7];
		}
	} else {
		if(switches[switchNum].lastState) {
			transmitObj.Data[1] = 0x02;
		} else {
			transmitObj.Data[1] = 0x00;
		}
		// send rule if present
		if(switches[switchNum].openRuleEnabled) {
			ruleDidSend = 1;
			ruleTransmitObj.ExtId = BUILD_ARB_FIELD(switches[switchNum].openRule.priority, 1, switches[switchNum].openRule.address, switches[switchNum].openRule.featureType, switches[switchNum].openRule.featureNumber, switches[switchNum].openRule.messageType);
			ruleTransmitObj.IDE = CAN_Id_Extended;
			ruleTransmitObj.RTR = CAN_RTR_Data;
			ruleTransmitObj.DLC = switches[switchNum].openRule.dlc;
			ruleTransmitObj.Data[0] = switches[switchNum].openRule.data[0];
			ruleTransmitObj.Data[1] = switches[switchNum].openRule.data[1];
			ruleTransmitObj.Data[2] = switches[switchNum].openRule.data[2];
			ruleTransmitObj.Data[3] = switches[switchNum].openRule.data[3];
			ruleTransmitObj.Data[4] = switches[switchNum].openRule.data[4];
			ruleTransmitObj.Data[5] = switches[switchNum].openRule.data[5];
			ruleTransmitObj.Data[6] = switches[switchNum].openRule.data[6];
			ruleTransmitObj.Data[7] = switches[switchNum].openRule.data[7];
		}
	}

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}

	if(ruleDidSend) {
		if(!transmitQueueFull()) {
			enqueueTransmitFrame(&ruleTransmitObj);
			primeTransmitBuffer();
		}
	}
}

void sendSwitchPolling(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x01);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = switches[switchNum].pollingInterval;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchPolling(uint8_t switchNum, uint8_t pollingInterval) {
	switches[switchNum].pollingInterval = pollingInterval;
}

void sendSwitchTriggering(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x02);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = switches[switchNum].messageTriggerMask;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchTriggering(uint8_t switchNum, uint8_t triggerMask) {
	switches[switchNum].messageTriggerMask = triggerMask;
}

void sendSwitchDebounce(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x03);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = switches[switchNum].debounceLimit;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchDebounce(uint8_t switchNum, uint8_t debounceLimit) {
	switches[switchNum].debounceLimit = debounceLimit;
}

void sendSwitchCloseParams(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x04);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 7;
	transmitObj.Data[0] = switches[switchNum].closeRuleEnabled;
	transmitObj.Data[1] = switches[switchNum].closeRule.priority;
	transmitObj.Data[2] = switches[switchNum].closeRule.address;
	transmitObj.Data[3] = switches[switchNum].closeRule.featureType;
	transmitObj.Data[4] = switches[switchNum].closeRule.featureNumber;
	transmitObj.Data[5] = switches[switchNum].closeRule.messageType;
	transmitObj.Data[6] = switches[switchNum].closeRule.dlc;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchCloseParams(uint8_t switchNum, uint8_t dlc, uint8_t enabled, uint8_t priority, uint8_t address, uint8_t featureType, uint8_t featureNumber, uint8_t messageType, uint8_t messageDlc) {
	switch(dlc) {
	case 8:
	case 7:
		switches[switchNum].closeRule.priority = priority;
		switches[switchNum].closeRule.address = address;
		switches[switchNum].closeRule.featureType = featureType;
		switches[switchNum].closeRule.featureNumber = featureNumber;
		switches[switchNum].closeRule.messageType = messageType;
		switches[switchNum].closeRule.dlc = messageDlc;
	case 6:
	case 5:
	case 4:
	case 3:
	case 2:
	case 1:
		switches[switchNum].closeRuleEnabled = enabled ? 1 : 0;
	default:
		break;
	}
}

void sendSwitchCloseData(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x05);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 8;
	memcpy((transmitObj.Data), switches[switchNum].closeRule.data, 8);

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchCloseData(uint8_t switchNum, uint8_t* data) {
	memcpy(switches[switchNum].closeRule.data, data, 8);
}

void sendSwitchOpenParams(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x06);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 7;
	transmitObj.Data[0] = switches[switchNum].openRuleEnabled;
	transmitObj.Data[1] = switches[switchNum].openRule.priority;
	transmitObj.Data[2] = switches[switchNum].openRule.address;
	transmitObj.Data[3] = switches[switchNum].openRule.featureType;
	transmitObj.Data[4] = switches[switchNum].openRule.featureNumber;
	transmitObj.Data[5] = switches[switchNum].openRule.messageType;
	transmitObj.Data[6] = switches[switchNum].openRule.dlc;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchOpenParams(uint8_t switchNum, uint8_t dlc, uint8_t enabled, uint8_t priority, uint8_t address, uint8_t featureType, uint8_t featureNumber, uint8_t messageType, uint8_t messageDlc) {
	switch(dlc) {
	case 8:
	case 7:
		switches[switchNum].openRule.priority = priority;
		switches[switchNum].openRule.address = address;
		switches[switchNum].openRule.featureType = featureType;
		switches[switchNum].openRule.featureNumber = featureNumber;
		switches[switchNum].openRule.messageType = messageType;
		switches[switchNum].openRule.dlc = messageDlc;
	case 6:
	case 5:
	case 4:
	case 3:
	case 2:
	case 1:
		switches[switchNum].openRuleEnabled = enabled ? 1 : 0;
	default:
		break;
	}
}

void sendSwitchOpenData(uint8_t priority, uint8_t switchNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, switchNum, 0x07);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 8;
	memcpy((transmitObj.Data), switches[switchNum].openRule.data, 8);

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSwitchOpenData(uint8_t switchNum, uint8_t* data) {
	memcpy(switches[switchNum].closeRule.data, data, 8);
}

void sendAllSwitchStatus(uint8_t priority) {
	CanTxMsg transmitObj, ruleTransmitObj;
	uint8_t ruleDidSend = 0;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x01, 0x00, 0x08);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 4;
	transmitObj.Data[0] = 0;
	transmitObj.Data[1] = 0;
	transmitObj.Data[2] = 0;
	transmitObj.Data[3] = 0;

	for(uint8_t switchNum = 0; switchNum < 16; switchNum++) {
		if(switches[switchNum].currentState) {
			if(switchNum < 8) {
				transmitObj.Data[0] |= (1 << switchNum);
			} else {
				transmitObj.Data[1] |= (1 << (switchNum - 8));
			}
		}

		if(!(switches[switchNum].currentState) != !(switches[switchNum].lastState) ) {
			if(switchNum < 8) {
				transmitObj.Data[2] |= (1 << switchNum);
			} else {
				transmitObj.Data[3] |= (1 << (switchNum - 8));
			}
		}
	}

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}


void processIncomingSwitchMatrixFrame(CanRxMsg *incomingMsg) {
	uint8_t priority, bs, address, featureType, featureNum, messageType, isRequest;
	decodeArbField(incomingMsg->ExtId, &priority, &bs, &address, &featureType, &featureNum, &messageType, &isRequest);
	isRequest = (incomingMsg->RTR) & CAN_RTR_Remote ? 1 : 0;

	switch(messageType) {
	case 0x00: // Switch status - requestable only
		if(isRequest) sendSwitchStatus(priority, featureNum);
		break;
	case 0x01: // Switch polling interval - set or requestable
		if(isRequest) {
			sendSwitchPolling(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSwitchPolling(featureNum, incomingMsg->Data[0]);
		}
		break;
	case 0x02: // Switch triggering - set or requestable
		if(isRequest) {
			sendSwitchTriggering(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSwitchTriggering(featureNum, incomingMsg->Data[0]);
		}
		break;
	case 0x03: // Switch debouncing - set or requestable
		if(isRequest) {
			sendSwitchDebounce(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSwitchDebounce(featureNum, incomingMsg->Data[0]);
		}
		break;
	case 0x04: // Switch rule - close event params
		if(isRequest) {
			sendSwitchCloseParams(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSwitchCloseParams(featureNum, incomingMsg->DLC, incomingMsg->Data[0], incomingMsg->Data[1], incomingMsg->Data[2], incomingMsg->Data[3], incomingMsg->Data[4], incomingMsg->Data[5], incomingMsg->Data[6]);
		}
		break;
	case 0x05: // Switch rule - close event data
		if(isRequest) {
			sendSwitchCloseData(priority, featureNum);
		} else {
			setSwitchCloseData(featureNum, incomingMsg->Data);
		}
		break;
	case 0x06: // Switch rule - open event params
		if(isRequest) {
			sendSwitchOpenParams(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSwitchOpenParams(featureNum, incomingMsg->DLC, incomingMsg->Data[0], incomingMsg->Data[1], incomingMsg->Data[2], incomingMsg->Data[3], incomingMsg->Data[4], incomingMsg->Data[5], incomingMsg->Data[6]);
		}
		break;
	case 0x07: // Switch rule - open event data
		if(isRequest) {
			sendSwitchOpenData(priority, featureNum);
		} else {
			setSwitchOpenData(featureNum, incomingMsg->Data);
		}
		break;
	case 0x08: // Switch status - all
		if(isRequest) {
			sendAllSwitchStatus(priority);
		}
		break;
	default:
		break;
	}

	return;
}

void sendLampStatus(uint8_t priority, uint8_t lampNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x02, lampNum, 0x00);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = lamps[lampNum].currentState;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setLampStatus(uint8_t lampNum, uint8_t dlc, uint8_t lampState, uint8_t onBright, uint8_t onDuration, uint8_t offBright, uint8_t offDuration) {
	lamps[lampNum].onState = lamps[lampNum].defaultOnState;
	lamps[lampNum].onTime = lamps[lampNum].defaultOnTime;
	lamps[lampNum].offState = lamps[lampNum].defaultOffState;
	lamps[lampNum].offTime = lamps[lampNum].defaultOffTime;

	switch(dlc) {
	case 8:
	case 7:
	case 6:
	case 5:
		lamps[lampNum].offTime = offDuration;
	case 4:
		lamps[lampNum].offState = offBright;
	case 3:
		lamps[lampNum].onTime = onDuration;
	case 2:
		lamps[lampNum].onState = onBright;
		lamps[lampNum].timeCounter = 0;
	case 1:
		lamps[lampNum].currentState = lampState;
	default:
		break;
	}

	buildLampFrameBuffer();

}

void setLampDefaults(uint8_t lampNum, uint8_t dlc, uint8_t onBright, uint8_t onDuration, uint8_t offBright, uint8_t offDuration) {
	switch(dlc) {
	case 8:
	case 7:
	case 6:
	case 5:
	case 4:
		lamps[lampNum].defaultOffTime = offDuration;
	case 3:
		lamps[lampNum].defaultOffState = offBright;
	case 2:
		lamps[lampNum].defaultOnTime = onDuration;
	case 1:
		lamps[lampNum].defaultOnState = onBright;

	default:
		break;
	}
}

void sendLampDefaults(uint8_t priority, uint8_t lampNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x02, lampNum, 0x01);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 4;
	transmitObj.Data[0] = lamps[lampNum].defaultOnState;
	transmitObj.Data[1] = lamps[lampNum].defaultOnTime;
	transmitObj.Data[2] = lamps[lampNum].defaultOffState;
	transmitObj.Data[3] = lamps[lampNum].defaultOffTime;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void sendLampTriggering(uint8_t priority, uint8_t lampNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x02, lampNum, 0x02);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = lamps[lampNum].messageTriggerMask;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setLampTriggering(uint8_t lampNum, uint8_t triggerMask) {
	lamps[lampNum].messageTriggerMask = triggerMask;
}

void processIncomingLampMatrixFrame(CanRxMsg *incomingMsg) {
	uint8_t priority, bs, address, featureType, featureNum, messageType, isRequest;
	decodeArbField(incomingMsg->ExtId, &priority, &bs, &address, &featureType, &featureNum, &messageType, &isRequest);
	isRequest = (incomingMsg->RTR) & CAN_RTR_Remote ? 1 : 0;

	switch(messageType) {
	case 0x00: // Lamp status - set or requestable
		if(isRequest) {
			sendLampStatus(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setLampStatus(featureNum, incomingMsg->DLC, incomingMsg->Data[0], incomingMsg->Data[1], incomingMsg->Data[2], incomingMsg->Data[3], incomingMsg->Data[4]);
		}
		break;
	case 0x01: // Lamp defaults - set or requestable
		if(isRequest) {
			sendLampDefaults(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setLampDefaults(featureNum, incomingMsg->DLC, incomingMsg->Data[0], incomingMsg->Data[1], incomingMsg->Data[2], incomingMsg->Data[3]);
		}
		break;
	case 0x02: // Lamp triggering - set or requestable
		if(isRequest) {
			sendLampTriggering(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setLampTriggering(featureNum, incomingMsg->Data[0]);
		}
		break;
	default:
		break;
	}

	return;
}

void updateSolenoidOutputs() {
	GPIO_WriteBit(SOLENOID0_PORT, SOLENOID0_PIN, !(solenoids[0].currentState));
	GPIO_WriteBit(SOLENOID1_PORT, SOLENOID1_PIN, !(solenoids[1].currentState));
	GPIO_WriteBit(SOLENOID2_PORT, SOLENOID2_PIN, !(solenoids[2].currentState));
	GPIO_WriteBit(SOLENOID3_PORT, SOLENOID3_PIN, !(solenoids[3].currentState));
	GPIO_WriteBit(SOLENOID4_PORT, SOLENOID4_PIN, !(solenoids[4].currentState));
	GPIO_WriteBit(SOLENOID5_PORT, SOLENOID5_PIN, !(solenoids[5].currentState));
	GPIO_WriteBit(SOLENOID6_PORT, SOLENOID6_PIN, !(solenoids[6].currentState));
	GPIO_WriteBit(SOLENOID7_PORT, SOLENOID7_PIN, !(solenoids[7].currentState));
}

void sendSolenoidStatus(uint8_t priority, uint8_t solenoidNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x03, solenoidNum, 0x00);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = solenoids[solenoidNum].currentState;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSolenoidStatus(uint8_t solenoidNum, uint8_t dlc, uint8_t solenoidStatus, uint8_t onTime) {
	solenoids[solenoidNum].onDuration = solenoids[solenoidNum].defaultOnDuration;
	switch(dlc) {
	case 8:
	case 7:
	case 6:
	case 5:
	case 4:
	case 3:
	case 2:
		solenoids[solenoidNum].onDuration = onTime;
	case 1:
		solenoids[solenoidNum].currentState = solenoidStatus;
		solenoids[solenoidNum].timeCounter = 0;
	default:
		break;
	}

	updateSolenoidOutputs();
}

void sendSolenoidDefaults(uint8_t priority, uint8_t solenoidNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x03, solenoidNum, 0x01);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = solenoids[solenoidNum].defaultOnDuration;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSolenoidDefaults(uint8_t solenoidNum, uint8_t dlc, uint8_t onTime) {
	switch(dlc) {
	case 8:
	case 7:
	case 6:
	case 5:
	case 4:
	case 3:
	case 2:
	case 1:
		solenoids[solenoidNum].defaultOnDuration = onTime;
	default:
		break;
	}
}

void sendSolenoidTriggering(uint8_t priority, uint8_t solenoidNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x03, solenoidNum, 0x02);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = solenoids[solenoidNum].messageTriggerMask;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setSolenoidTriggering(uint8_t solenoidNum, uint8_t triggerMask) {
	solenoids[solenoidNum].messageTriggerMask = triggerMask;
}

void processIncomingSolenoidFrame(CanRxMsg *incomingMsg) {
	uint8_t priority, bs, address, featureType, featureNum, messageType, isRequest;
	decodeArbField(incomingMsg->ExtId, &priority, &bs, &address, &featureType, &featureNum, &messageType, &isRequest);
	isRequest = (incomingMsg->RTR) & CAN_RTR_Remote ? 1 : 0;

	switch(messageType) {
	case 0x00: // Solenoid status - set or requestable
		if(isRequest) {
			sendSolenoidStatus(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSolenoidStatus(featureNum, incomingMsg->DLC, incomingMsg->Data[0], incomingMsg->Data[1]);
		}
		break;
	case 0x01: // Solenoid defaults - set or requestable
		if(isRequest) {
			sendSolenoidDefaults(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSolenoidDefaults(featureNum, incomingMsg->DLC, incomingMsg->Data[0]);
		}
		break;
	case 0x02: // Lamp triggering - set or requestable
		if(isRequest) {
			sendSolenoidTriggering(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setSolenoidTriggering(featureNum, incomingMsg->Data[0]);
		}
		break;
	default:
		break;
	}

	return;
}


void sendRGBStatus(uint8_t priority, uint8_t rgbNum) {
	CanTxMsg transmitObj;

	transmitObj.ExtId = BUILD_ARB_FIELD(priority, 1, boardAddress, 0x05, rgbNum, 0x00);
	transmitObj.IDE = CAN_Id_Extended;
	transmitObj.RTR = CAN_RTR_Data;
	transmitObj.DLC = 1;
	transmitObj.Data[0] = rgbLEDs[rgbNum].red;
	transmitObj.Data[0] = rgbLEDs[rgbNum].green;
	transmitObj.Data[0] = rgbLEDs[rgbNum].blue;

	if(!transmitQueueFull()) {
		enqueueTransmitFrame(&transmitObj);
		primeTransmitBuffer();
	}
}

void setRGBStatus(uint8_t rgbNum, uint8_t dlc, uint8_t red, uint8_t green, uint8_t blue) {
	switch(dlc) {
	case 8:
	case 7:
	case 6:
	case 5:
	case 4:
	case 3:
		rgbLEDs[rgbNum].red = red;
		rgbLEDs[rgbNum].green = green;
		rgbLEDs[rgbNum].blue = blue;
	case 2:
	case 1:
	default:
		break;
	}

	pixels_buildSPIArray();
	pixels_sendUpdate();

}

void processIncomingRGBFrame(CanRxMsg *incomingMsg) {
	uint8_t priority, bs, address, featureType, featureNum, messageType, isRequest;
	decodeArbField(incomingMsg->ExtId, &priority, &bs, &address, &featureType, &featureNum, &messageType, &isRequest);
	isRequest = (incomingMsg->RTR) & CAN_RTR_Remote ? 1 : 0;

	switch(messageType) {
	case 0x00: // Lamp status - set or requestable
		if(isRequest) {
			sendRGBStatus(priority, featureNum);
		} else {
			if(incomingMsg->DLC >= 1) setRGBStatus(featureNum, incomingMsg->DLC, incomingMsg->Data[0], incomingMsg->Data[1], incomingMsg->Data[2]);
		}
		break;
	default:
		break;
	}

	return;
}


void processIncomingFrame(CanRxMsg *incomingMsg) {
	uint8_t priority, bs, address, featureType, featureNum, messageType, isRequest;
	decodeArbField(incomingMsg->ExtId, &priority, &bs, &address, &featureType, &featureNum, &messageType, &isRequest);
	isRequest = (incomingMsg->RTR) & CAN_RTR_Remote ? 1 : 0;

	if(!((address == boardAddress) || (!bs))) return;

	switch(featureType) {
	case 0x00: // System Management
		processIncomingSystemManagementFrame(incomingMsg);
		break;
	case 0x01: // Switch Matrix
		processIncomingSwitchMatrixFrame(incomingMsg);
		break;
	case 0x02: // Lamp Matrix
		processIncomingLampMatrixFrame(incomingMsg);
		break;
	case 0x03: // Solenoids
		processIncomingSolenoidFrame(incomingMsg);
		break;
	case 0x04: // Score Display - not implemented on this board
		break;
	case 0x05: // RGB LEDs
		processIncomingRGBFrame(incomingMsg);
		break;
	default:
		break;
	}
}

static void CAN_Config(uint8_t canSpeed)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  NVIC_InitTypeDef  NVIC_InitStructure;
  CAN_InitTypeDef        CAN_InitStructure;
  CAN_FilterInitTypeDef  CAN_FilterInitStructure;

  /* CAN GPIOs configuration **************************************************/

  /* Enable GPIO clock */
  RCC_AHBPeriphClockCmd(CAN_GPIO_CLK, ENABLE);

  /* Connect CAN pins to AF7 */
  GPIO_PinAFConfig(CAN_GPIO_PORT, CAN_RX_SOURCE, CAN_AF_PORT);
  GPIO_PinAFConfig(CAN_GPIO_PORT, CAN_TX_SOURCE, CAN_AF_PORT);

  /* Configure CAN RX and TX pins */
  GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN | CAN_TX_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(CAN_GPIO_PORT, &GPIO_InitStructure);

  /* NVIC configuration *******************************************************/
  NVIC_InitStructure.NVIC_IRQChannel = CEC_CAN_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* CAN configuration ********************************************************/
  /* Enable CAN clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN, ENABLE);

  /* CAN register init */
  CAN_DeInit(CAN);
  CAN_StructInit(&CAN_InitStructure);

  /* CAN cell init */
  CAN_InitStructure.CAN_TTCM = DISABLE;
  CAN_InitStructure.CAN_ABOM = DISABLE;
  CAN_InitStructure.CAN_AWUM = DISABLE;
  CAN_InitStructure.CAN_NART = DISABLE;
  CAN_InitStructure.CAN_RFLM = DISABLE;
  CAN_InitStructure.CAN_TXFP = DISABLE;
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
  CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;

  if(canSpeed == 0x03) {
	  /* CAN Baudrate = 1000kBps (CAN clocked at 48 MHz) */
	  CAN_InitStructure.CAN_Prescaler = 3;
  } else if(canSpeed == 0x02) {
	  /* CAN Baudrate = 500kBps (CAN clocked at 48 MHz) */
	  CAN_InitStructure.CAN_Prescaler = 6;
  } else if(canSpeed == 0x01) {
	  /* CAN Baudrate = 250kBps (CAN clocked at 48 MHz) */
	  CAN_InitStructure.CAN_Prescaler = 12;
  } else {
	  /* CAN Baudrate = 125kBps (CAN clocked at 48 MHz) */
	  CAN_InitStructure.CAN_Prescaler = 24;
  }
  CAN_InitStructure.CAN_BS1 = CAN_BS1_13tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;
  CAN_Init(CAN, &CAN_InitStructure);

  uint32_t boardSpecificAddress = BUILD_ARB_FIELD(0, 1, boardAddress, 0, 0, 0);
  uint16_t boardSpecificAddressHigh = (boardSpecificAddress >> 16UL);
  /* CAN filter init */
  CAN_FilterInitStructure.CAN_FilterNumber = 0;
  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = boardSpecificAddressHigh;
  CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x01FF;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);

  CAN_FilterInitStructure.CAN_FilterNumber = 1;
  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
  CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0100;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);

  /* Enable FIFO 0 message pending Interrupt */
  CAN_ITConfig(CAN, CAN_IT_FMP0, ENABLE);
  CAN_ITConfig(CAN, CAN_IT_TME, ENABLE);
}

void gpio_init(void);

void power_init(void) {
	GPIO_WriteBit(VSOL_PRECHARGE_PORT, VSOL_PRECHARGE_PIN, 1);
	//GPIO_WriteBit(POWERGOOD_PORT, POWERGOOD_PIN, 0);
}

int main(void) {
	uint8_t i = 0;

	uint8_t canSpeed = 0;

	SystemInit();
	SystemCoreClockUpdate();

	//GPIOInit();
	gpio_init();

	setup_io();

	power_init();

	boardAddress = read_dip_switch();
	canSpeed = (boardAddress & 0b11000000) >> 6;
	boardAddress = boardAddress & 0b00111111;

	CAN_Config(canSpeed);

	setup_timer();

	initializeLamps();
	initializeSwitches();
	initializeSolenoids();
	initializeRGB();
/*	rgbLEDs[0].red = 255;
	rgbLEDs[1].blue = 255;
	rgbLEDs[2].green = 255;
	rgbLEDs[3].red = 127;
	rgbLEDs[4].blue = 127;
	rgbLEDs[5].green = 127;
	rgbLEDs[6].red = 63;
	rgbLEDs[7].blue = 63;
	rgbLEDs[8].green = 63;
	rgbLEDs[9].red = 31;
	rgbLEDs[10].blue = 31;
	rgbLEDs[11].green = 31;
	rgbLEDs[12].red = 15;
	rgbLEDs[13].blue = 15;
	rgbLEDs[14].green = 15;
	rgbLEDs[15].red = 16;*/
	pixels_buildSPIArray();
	pixels_sendUpdate();

	sendBoardID1(0x0F);
	sendBoardID2(0x0F);

	clearLampBuffer();
	buildLampFrameBuffer();

	while(1) {
		// Process heartbeat
		if(heartbeatCounter > 500) {
			GPIO_WriteBit(HEARTBEAT_PORT, HEARTBEAT_PIN, 1);
		} else {
			GPIO_WriteBit(HEARTBEAT_PORT, HEARTBEAT_PIN, 0);
		}

		while(!receiveQueueEmpty()) {
			dequeueReceivedFrame(&msg_obj);
			processIncomingFrame(&msg_obj);
		}

		if(ledMatrixFlag) {
			// Update LED matrix
			writeLEDMatrix(lampFrameBuffer[ledMatrixCol]);
			ledMatrixFlag = 0;
		}

		if(keyMatrixFlag) {
			// Read row of key matrix
			readSwitchCol(keyMatrixCol);
			keyMatrixFlag = 0;
		}

		if(solenoidFlag) {
			uint8_t updateSolenoidFlag = 0;
			// Decrement time remaining on affected solenoids, turn off if expired
			for(i=0; i<8; i++) {
				if(solenoids[i].currentState) {
					if(solenoids[i].onDuration > 0) {
						solenoids[i].timeCounter++;
						if(solenoids[i].timeCounter >= solenoids[i].onDuration) {
							solenoids[i].currentState = 0;
							updateSolenoidFlag = 1;
							if(solenoids[i].messageTriggerMask) {
								sendSolenoidStatus(0x0B, i);
							}
						}
					}
				}
			}

			if(updateSolenoidFlag) updateSolenoidOutputs();
			updateSolenoidFlag = 0;
			solenoidFlag = 0;
		}

		if(ledBlinkFlag) {
			uint8_t updateBufferFlag = 0;

			for(i=0; i<16; i++) {
				if((lamps[i].onTime > 0) && (lamps[i].offTime > 0)) {
					lamps[i].timeCounter++;
					if(lamps[i].currentState) {
						if(lamps[i].timeCounter >= lamps[i].onTime) {
							lamps[i].timeCounter = 0;
							lamps[i].currentState = 0;
							updateBufferFlag = 1;
							if(lamps[i].messageTriggerMask) {
								sendLampStatus(0x0D, i);
							}
						}
					} else {
						if(lamps[i].timeCounter >= lamps[i].offTime) {
							lamps[i].timeCounter = 0;
							lamps[i].currentState = 1;
							updateBufferFlag = 1;
							if(lamps[i].messageTriggerMask) {
								sendLampStatus(0x0D, i);
							}
						}
					}
				}
			}

			if(updateBufferFlag) buildLampFrameBuffer();
			updateBufferFlag = 0;
			ledBlinkFlag = 0;
		}

		if(switchPollingFlag) {
			for(i=0; i<16; i++) {
				if(switches[i].pollingInterval != 0) {
					switches[i].pollingCounter++;
					if(switches[i].pollingCounter >= switches[i].pollingInterval) {
						sendSwitchStatus(0x01, i);
						switches[i].pollingCounter = 0;
					}
				}
			}
			switchPollingFlag = 0;
		}

		if(powerStatusPollingFlag) {
			if(powerStatusPollingInterval != 0) {
				powerStatusPollingCount++;
				if(powerStatusPollingCount >= powerStatusPollingInterval) {
					sendPowerStatus(0x0E);
					powerStatusPollingCount = 0;
				}
			}

			powerStatusPollingFlag = 0;
		}
	}
	return 0 ;
}

static void small_delay(int delay)
{
    delay = delay * 10;
    while (delay > 0) {
        delay--;
    }
}

void gpio_init() {
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
}

void setup_io() {
	GPIO_InitTypeDef initStruct;

	// Set up heartbeat pin
	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = HEARTBEAT_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(HEARTBEAT_PORT, &initStruct);
	GPIO_ResetBits(HEARTBEAT_PORT, HEARTBEAT_PIN);

	// Set up power good pin
	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = POWERGOOD_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(POWERGOOD_PORT, &initStruct);
	GPIO_ResetBits(POWERGOOD_PORT, POWERGOOD_PIN);

	// Set up VSOL precharge pin
	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = VSOL_PRECHARGE_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(VSOL_PRECHARGE_PORT, &initStruct);
	GPIO_ResetBits(VSOL_PRECHARGE_PORT, VSOL_PRECHARGE_PIN);

	// Set up lamp matrix pins
	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = LAMP_ROW0_PIN | LAMP_ROW1_PIN | LAMP_ROW2_PIN | LAMP_ROW3_PIN | LAMP_COL0_PIN | LAMP_COL1_PIN | LAMP_COL2_PIN | LAMP_COL3_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LAMP_ROW0_PORT, &initStruct);
	GPIO_ResetBits(LAMP_ROW0_PORT, LAMP_ROW0_PIN | LAMP_ROW1_PIN | LAMP_ROW2_PIN | LAMP_ROW3_PIN);
	GPIO_SetBits(LAMP_COL0_PORT, LAMP_COL0_PIN | LAMP_COL1_PIN | LAMP_COL2_PIN | LAMP_COL3_PIN);

	// Set up solenoid outputs
	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = SOLENOID0_PIN | SOLENOID1_PIN | SOLENOID2_PIN | SOLENOID3_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SOLENOID0_PORT, &initStruct);
	GPIO_SetBits(SOLENOID0_PORT, SOLENOID0_PIN | SOLENOID1_PIN | SOLENOID2_PIN | SOLENOID3_PIN);

	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = SOLENOID4_PIN | SOLENOID5_PIN | SOLENOID6_PIN | SOLENOID7_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SOLENOID4_PORT, &initStruct);
	GPIO_SetBits(SOLENOID4_PORT, SOLENOID4_PIN | SOLENOID5_PIN | SOLENOID6_PIN | SOLENOID7_PIN);

	// Set up switch matrix pins
	initStruct.GPIO_Mode = GPIO_Mode_OUT;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = SWITCH_COL0_PIN | SWITCH_COL1_PIN | SWITCH_COL2_PIN | SWITCH_COL3_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SWITCH_COL0_PORT, &initStruct);
	GPIO_SetBits(SWITCH_COL0_PORT, SWITCH_COL0_PIN | SWITCH_COL1_PIN | SWITCH_COL2_PIN | SWITCH_COL3_PIN);

	initStruct.GPIO_Mode = GPIO_Mode_IN;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = SWITCH_ROW0_PIN | SWITCH_ROW1_PIN | SWITCH_ROW2_PIN | SWITCH_ROW3_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SWITCH_ROW0_PORT, &initStruct);

	// Set up DIP Switch pins
	initStruct.GPIO_Mode = GPIO_Mode_IN;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = ADDRESS0_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(ADDRESS0_PORT, &initStruct);

	initStruct.GPIO_Mode = GPIO_Mode_IN;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = ADDRESS1_PIN | ADDRESS2_PIN | ADDRESS3_PIN | ADDRESS7_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(ADDRESS1_PORT, &initStruct);

	initStruct.GPIO_Mode = GPIO_Mode_IN;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = ADDRESS4_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(ADDRESS4_PORT, &initStruct);

	initStruct.GPIO_Mode = GPIO_Mode_IN;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = ADDRESS5_PIN | ADDRESS6_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(ADDRESS5_PORT, &initStruct);

	// RGB

	initStruct.GPIO_Mode = GPIO_Mode_AF;
	initStruct.GPIO_OType = GPIO_OType_PP;
	initStruct.GPIO_Pin = RGB_OUT_PIN;
	initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	initStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(RGB_OUT_PORT, &initStruct);

	GPIO_PinAFConfig(RGB_OUT_PORT, RGB_OUT_SOURCE, RGB_OUT_AF_PORT);

	SPI_InitTypeDef SPI_InitStructure;

	/* SPI Config */
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; // 8 MHz SPI, so 4 SPI bits = 1 WS2812B bit
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;

	SPI_Init(SPI1, &SPI_InitStructure);

	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET) ;

	DMA_InitTypeDef DMA_InitStructure;

	DMA_DeInit(DMA1_Channel3);

	DMA_InitStructure.DMA_BufferSize = NUM_PIXEL_SPI_BYTES;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(SPI1->DR));
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;

	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) pixelSPIArray;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel3, &DMA_InitStructure);

	DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel3, DMA_IT_TE, ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Set up ADC inputs

	return;
}

uint32_t read_dip_switch() {
	uint32_t retVal = 0;

	retVal = (1 * GPIO_ReadInputDataBit(ADDRESS0_PORT, ADDRESS0_PIN))
			+(2 * GPIO_ReadInputDataBit(ADDRESS1_PORT, ADDRESS1_PIN))
			+(4 * GPIO_ReadInputDataBit(ADDRESS2_PORT, ADDRESS2_PIN))
			+(8 * GPIO_ReadInputDataBit(ADDRESS3_PORT, ADDRESS3_PIN))
			+(16 * GPIO_ReadInputDataBit(ADDRESS4_PORT, ADDRESS4_PIN))
			+(32 * GPIO_ReadInputDataBit(ADDRESS5_PORT, ADDRESS5_PIN))
			+(64 * GPIO_ReadInputDataBit(ADDRESS6_PORT, ADDRESS6_PIN))
			+(128 * GPIO_ReadInputDataBit(ADDRESS7_PORT, ADDRESS7_PIN));

	return retVal;
}

void setup_timer() {
	/* Time base configuration */
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	/* Enable the TIM3 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseStructure.TIM_Period = (SystemCoreClock/1000 - 1);
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* TIM Interrupts enable */
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM3, ENABLE);

	return;
}

volatile uint8_t isrCount = 0;
volatile uint8_t solenoidCount = 0;
volatile uint8_t powerStatusCount = 0;
volatile uint8_t switchPollingCount = 0;
volatile uint8_t ledBlinkCount = 0;

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	  {
	    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);			/* clear interrupt flag */
		heartbeatCounter++;
		if(heartbeatCounter > 1000) {
			heartbeatCounter = 0;
		}

		if(isrCount == 0) {
			ledMatrixCol++;
			ledMatrixCol &= 0x03;
			ledMatrixFlag = 1;
			isrCount = 1;
		} else {
			keyMatrixCol++;
			keyMatrixCol &= 0x03;
			keyMatrixFlag = 1;
			isrCount = 0;
		}

		solenoidCount++;
		if(solenoidCount >= 10) {
			solenoidCount = 0;
			solenoidFlag = 1;
		}

		switchPollingCount++;
		if(switchPollingCount >= 10) {
			switchPollingCount = 0;
			switchPollingFlag = 1;
		}

		powerStatusCount++;
		if(powerStatusCount >= 10) {
			powerStatusCount = 0;
			powerStatusPollingFlag = 1;
		}

		ledBlinkCount++;
		if(ledBlinkCount >= 10) {
			ledBlinkCount = 0;
			ledBlinkFlag = 1;
		}
	}

	return;
}

void DMA1_Channel2_3_IRQHandler(void) {
	if(DMA_GetITStatus(DMA1_IT_TC3)) {
		dmaComplete = 1;
		DMA_ClearITPendingBit(DMA1_IT_GL3 | DMA1_IT_TC3);
		DMA_Cmd(DMA1_Channel3, DISABLE);
		SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Tx, DISABLE);
		SPI_Cmd(SPI1, DISABLE);
	} else if (DMA_GetITStatus(DMA1_IT_TE3)) {
		dmaComplete = 1;
		DMA_ClearITPendingBit(DMA1_IT_GL3 | DMA1_IT_TC3);
		DMA_Cmd(DMA1_Channel3, DISABLE);
		SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Tx, DISABLE);
		SPI_Cmd(SPI1, DISABLE);
	}
	return;
}
